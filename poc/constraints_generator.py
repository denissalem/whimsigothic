#! /usr/bin/env python3

import os
import random

from PIL import Image

TILE_SCALE = 32
CONSTRAINTS_SRC_FILE = "../drafts/40tiles.png" # TODO EXTRACT NUMBER OF ACTUAL TILES

class Constraint:
    TYPE_UNDEFINED = 0
    TYPE_WALL = 1
    TYPE_HOLE = 2
    def __init__(self, identifier, boundaries):
        self.identifier = identifier
        self.boundaries = boundaries
        self.type = () # TODO : Tuple of 8 TYPE, define on boundaries analysis.

        self.top_left = []
        self.top = []
        self.top_right = []
        self.left = []
        self.right = []
        self.bottom_left = []
        self.bottom = []
        self.bottom_right = []

class Pattern:
    def __init__(constraint, variation_name, image):
        self.constraint = constraint
        self.image = image
        self.variation_name = variation_name
        self.constraints_variation = [] # Must be manually defined by end user

def get_tile_bondaries(tile_scale, constraints_image, logic_x, logic_y):
    return (
        [ constraints_image.getpixel((logic_x*tile_scale+x, logic_y*tile_scale))              for x in range(0, tile_scale)], # TOP
        [ constraints_image.getpixel((logic_x*tile_scale+x, logic_y*tile_scale+tile_scale-1)) for x in range(0, tile_scale)], # BOTTOM
        [ constraints_image.getpixel((logic_x*tile_scale, logic_y*tile_scale+y))              for y in range(0, tile_scale)], # LEFT
        [ constraints_image.getpixel((logic_x*tile_scale+tile_scale-1,logic_y*tile_scale+y))  for y in range(0, tile_scale)], # RIGHT
    )

def test_side_match(tile_scale, side1, side2):
    match = 0
    for i in range(0, tile_scale):
        if side1[i] == side2[i]:
            match += 1
            
    return (tile_scale - match) <= 1

def build_constraints_set(constraints_src_file, tile_scale):
    constraints_image = Image.open(constraints_src_file)
    constraints = []
    constraints_append = constraints.append
    
    for logic_y in range(0, constraints_image.size[1] // tile_scale):
        for logic_x in range(0, constraints_image.size[0] // tile_scale):
            constraints_append(
                Constraint(
                    "{:02x}".format(logic_x)+"{:02x}".format(logic_y),
                    get_tile_bondaries(tile_scale, constraints_image, logic_x, logic_y)
                )
            )

    for constraint in constraints:
        for candidate in constraints:
            if candidate != constraint:
                if test_side_match(tile_scale, constraint.boundaries[0], candidate.boundaries[1]):
                    constraint.top.append(candidate.identifier)
                    
                if test_side_match(tile_scale, constraint.boundaries[1], candidate.boundaries[0]):
                    constraint.bottom.append(candidate.identifier)
                    
                if test_side_match(tile_scale, constraint.boundaries[2], candidate.boundaries[3]):
                    constraint.left.append(candidate.identifier)
            
                if test_side_match(tile_scale, constraint.boundaries[3], candidate.boundaries[2]):
                    constraint.right.append(candidate.identifier)
    

    return constraints

constraints = build_constraints_set(CONSTRAINTS_SRC_FILE, TILE_SCALE)
