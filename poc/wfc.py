#! /usr/bin/env python3

from PIL import Image

from constraints_generator import build_constraints_set

TILE_SCALE = 32
CONSTRAINTS_SRC_FILE = "../drafts/40tiles.png"
PATTERN_SRC_FILE = "../drafts/40tiles_with_grid.png"

class Pattern:
    def __init__(self, constraint, variation_name, image):
        self.constraint = constraint
        self.image = image
        self.variation_name = variation_name
        self.constraints_variation = [] # Must be manually defined by end user
        
def get_pattern_image(patterns_image, logic_x, logic_y, tile_scale):
    image = Image.new('RGBA', (tile_scale, tile_scale))
    buf = []
    for y in range(0, tile_scale):
        for x in range(0, tile_scale):
            image.putpixel(
                (x,y),
                patterns_image.getpixel((logic_x*tile_scale+x, logic_y*tile_scale+y))
            )
            
    return image
    
def load_patterns(asset_path, tile_scale, patterns_database):
    patterns_image = Image.open(asset_path).convert('RGBA')
    patterns_database[asset_path] = {}

    for logic_y in range(0, patterns_image.size[1] // tile_scale):
        for logic_x in range(0, patterns_image.size[0] // tile_scale):
            identifier = "{:02x}".format(logic_x)+"{:02x}".format(logic_y)
            patterns_database[asset_path][identifier] = Pattern(
                constraints[identifier],
                asset_path,
                get_pattern_image(patterns_image, logic_x, logic_y, tile_scale)
            )
            
constraints = build_constraints_set(CONSTRAINTS_SRC_FILE, TILE_SCALE)

patterns_database = {}

load_patterns(PATTERN_SRC_FILE, TILE_SCALE, patterns_database)

patterns_database[PATTERN_SRC_FILE]["0601"].image.show()
